document.addEventListener('deviceready', function() {
    onDeviceReady();
}, false);

function onDeviceReady() {
    document.addEventListener("backbutton", function() {
        angular.element('[ng-controller=AppCtrl]').scope().back();
        //alert('back!');
    }, false);
}

var app = angular.module('app', ['ngMaterial', 'ngRoute', 'ngTouch', 'ngSanitize']);

app.config(['$routeProvider', function($routeProvider) {
    $routeProvider
        .when('/boards', {templateUrl: 'template/boards.html'})
        .when('/board/:board', {templateUrl: 'template/board.html'})
        .when('/board/:board/thread/:no', {templateUrl: 'template/thread.html'})
        .otherwise({redirectTo:'/boards'})
}]);

app.run(['$rootScope', '$location', function($rootScope, $location) {
    $rootScope.$on('$routeChangeStart', function(a, b) {
        $rootScope.routeChange = b;
    });
}]);

app.controller('AppCtrl', ['$scope', '$mdSidenav', '$http', '$q', '$rootScope', '$location', function($scope, $mdSidenav, $http, $q, $rootScope, $location) {
    $rootScope.title = "ng-chan";
    $scope.navBtn = "menuBtn";

    if($rootScope.boards == undefined) {
        var promise = loadBoards();
        promise.then(function(data) {
            $rootScope.boards = data;
        }, function(msg) {
            alert(msg);
        });
    }

    $scope.toggleSidenav = function(menuId) {
        $mdSidenav(menuId).toggle();
    };

    function loadBoards() {
        return $q(function(resolve, reject) {
            var url = "js/boards.json";
            $http.get(url)
                .success(function(data) {
                    resolve(data);
                })
                .error(function() {
                    reject('Boards unreachful');
                });
        });
    }

    $scope.back = function() {
        $rootScope.title = "ng-chan";
    };

    $scope.navButton = function () {
        var path = $rootScope.routeChange.$$route.originalPath;
        if(path == "/boards") {
            $scope.toggleSidenav('left');
        } else if(path == "/board/:board"){
            $location.path('/');
            $rootScope.title = "ng-chan";
        } else if(path == "/board/:board/thread/:no") {
            $location.path('/board/'+$rootScope.routeChange.pathParams.board);
            $rootScope.title = "ng-chan";
        } else {
            alert('Error 08');
            $location.path('/');
        }
    };

    $rootScope.$on("navigationButton", function(event, args) {
        if(args.value = "back") {
            $scope.navBtn = "backBtn";
        } else {
            $scope.navBtn = "menuBtn";
        }
    });

}]);
