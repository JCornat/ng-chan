var app = angular.module('app');

app.controller('ThreadCtrl', ['$scope', '$routeParams', '$q', '$http', '$rootScope', '$location', function($scope, $routeParams, $q, $http, $rootScope, $location) {

    $rootScope.currentPath = $location.path();
    var id = $routeParams.no;
    var board = $routeParams.board;
    $scope.limit = 3;
    $scope.limitBtn = "MOAR";
    $scope.limitReplies = $scope.limit;

    var promise = loadReplies();
    promise.then(function(resolve) {
        $scope.replies = [];
        $.each(resolve, function(a,b) {
            $.each(b, function(c, d) {
                if(c > 0 && d.tim != undefined && (d.ext != '.webm' && d.ext != undefined)) {
                    d.image = 'https://i.4cdn.org/'+board+'/'+d.tim+d.ext;
                    $scope.replies.push(d);
                }
            });
        });
    }, function(reject) {

    });

    function loadReplies() {
        return $q(function(resolve, reject) {
            var url = "https://a.4cdn.org/"+board+"/thread/"+id+".json";
            $http.get(url)
                .success(function(data) {
                    resolve(data);
                })
                .error(function() {
                    reject('Threads unreachful');
                });
        });
    }

    $scope.loadMore = function() {
        $scope.limitReplies += $scope.limit;
        if($scope.limitReplies >= $scope.replies) {

        }
    }
}]);