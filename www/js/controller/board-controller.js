var app = angular.module('app');

app.controller('BoardCtrl', ['$scope', '$routeParams', '$q', '$http', '$rootScope', '$location', '$mdToast', function($scope, $routeParams, $q, $http, $rootScope, $location, $mdToast) {
    $scope.loading = true;
    var id = $routeParams.board;
    if($rootScope.currentBoard == undefined || id != $rootScope.currentBoard.board) {
        $rootScope.$broadcast("navigationButton", {value:"back"});

        $.each($rootScope.boards.boards, function(i, v) {
            if (v.board == id) {
                $rootScope.currentBoard = v;
                $rootScope.title = "/"+$rootScope.currentBoard.board+"/ - "+$rootScope.currentBoard.title;
                return;
            }
        });

        var promise = loadThreads();
        promise.then(function (data) {

            $rootScope.threads = [];
            $.each(data, function(a,b) {
                $.each(b.threads, function(a,b) {
                    if(b.tim != undefined && (b.ext != '.webm' && b.ext != undefined)) {
                        b.image = 'http://i.4cdn.org/'+$rootScope.currentBoard.board+'/'+b.tim+b.ext;
                        $rootScope.threads.push(b);
                    }
                });
            });
            $rootScope.threadPos = 0;
            $rootScope.thread = loadThread();
        }, function (msg) {
            alert(msg);
        });
    } else {
        $rootScope.title = "/"+$rootScope.currentBoard.board+"/ - "+$rootScope.currentBoard.title;
        $scope.loading = true;
        var promise2 = loadImage($rootScope.threads[$rootScope.threadPos]);
        promise2.then(function() {
            $rootScope.thread = $rootScope.threads[$rootScope.threadPos];
            $scope.loading = false;
        }, function() {
            alert("Error 07");
            $scope.loading = false;
        });
    }

    $scope.onSwipeLeft = function () {
        $rootScope.threadPos++;
        $rootScope.thread = loadThread();
    };

    $scope.clickSwipeLeft = function() {
        $mdToast.show(
            $mdToast.simple()
                .content('Pro Tip : Try to swipe from the right side to the left ;)')
                .position($scope.getToastPosition())
                .hideDelay(4000)
        );
        $scope.onSwipeLeft();
    };

    $scope.clickSwipeRight = function() {
        $mdToast.show(
            $mdToast.simple()
                .content('Pro Tip : Try to swipe from the left side to the right ;)')
                .position($scope.getToastPosition())
                .hideDelay(4000)
        );
        $scope.onSwipeRight();
    };

    $scope.toastPosition = {
        bottom: false,
        top: true,
        left: false,
        right: true
    };

    $scope.getToastPosition = function() {
        return Object.keys($scope.toastPosition)
            .filter(function(pos) { return $scope.toastPosition[pos]; })
            .join(' ');
    };

    $scope.onSwipeRight = function () {
        if($rootScope.threadPos > 0) {
            $rootScope.threadPos--;
        }
        $rootScope.thread = loadThread();
    };

    function loadImage(thread) {
        return $q(function(resolve, reject) {
            var imageLoading = new Image();
            imageLoading.onload = function () {
                resolve(thread);
            };
            imageLoading.onerror = function () {
                reject();
            };
            imageLoading.src = thread.image;
        });
    }

    function loadThreads() {
        return $q(function(resolve, reject) {
            var url = "https://a.4cdn.org/"+$rootScope.currentBoard.board+"/catalog.json";
            $http.get(url)
                .success(function(data) {
                    resolve(data);
                })
                .error(function() {
                    reject('Threads unreachful');
                });

        });
    }

    function loadThread() {
        $scope.loading = true;
        var promise2 = loadImage($rootScope.threads[$rootScope.threadPos]);
        promise2.then(function() {
            $rootScope.thread = $rootScope.threads[$rootScope.threadPos];
            $scope.loading = false;
        }, function() {
            alert("Error 07");
            $scope.loading = false;
        });
    }

    $scope.loadReplies = function() {
        $location.path('/board/'+$rootScope.currentBoard.board+'/thread/'+$rootScope.thread.no);
    };

}]);
